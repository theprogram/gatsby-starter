export const Nav = Object.freeze({
  Home: '/',
  About: '/about',
  Contact: '/contact',
});
