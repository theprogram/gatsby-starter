import * as React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/Layout';
import Section from '../components/Section';


const Home = ({ data: { home } }) => {
  const [hero, ...content] = home.content;

  return (
    <Layout
      path="/"
      hero={hero}
    >
      {content.map((item, i) => (
        <Section
          {...item}
          key={i}
        />
      ))}
    </Layout>
  );
};

const data = graphql`
  query Home {
    home: contentfulPage(contentful_id: {eq: "1jVi9ttigyt99L8TT8nQXB"}) {
      title
      description
      content {
        heading
        copy {
          copy
        }
        asset {
          desktop {
            file {
              url
            }
            description
          }
          mobile {
            file {
              url
            }
          }
        }
      }
    }
  }
`;


export default Home;
export { data };
