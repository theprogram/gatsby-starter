import * as React from 'react';
import { shape, string } from 'prop-types';
import * as styles from './ResImage.module.scss';


const ResImage = ({
  desktop,
  mobile,
  text,
}) => {
  if (!desktop) {
    return null;
  }

  return (
    <picture className={styles.root}>
      {mobile && (
        <>
          <source
            media="(max-width:1023px)"
            srcSet={mobile.file.url}
          />
          <source
            media="(min-width:1024px)"
            srcSet={desktop.file.url}
          />
        </>
      )}
      <img
        src={desktop.file.url}
        alt={text ?? desktop.description}
      />
    </picture>
  );
};

ResImage.propTypes = {
  desktop: shape({
    file: shape({
      url: string,
      description: string,
    }),
  }),
  mobile: shape({ file: shape({ url: string }) }),
  text: string,
};


export default ResImage;
