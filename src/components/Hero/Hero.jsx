import * as React from 'react';
import {
  object, shape, string,
} from 'prop-types';
import ResImage from '../ResImage';
import * as styles from './Hero.module.scss';


const Hero = ({
  heading,
  copy,
  asset,
}) => (
  <header className={styles.root}>
    <ResImage {...asset} />
    <h1>Gatsby { heading }</h1>
    {copy.copy && <p>{ copy.copy }</p>}
  </header>
);

Hero.propTypes = {
  heading: string,
  copy: shape({ copy: string.isRequired }),
  asset: shape({
    desktop: object.isRequired,
    mobile: object,
  }),
};

Hero.defaultProps = { heading: 'Hello World' };


export default Hero;
