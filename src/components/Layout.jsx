import * as React from 'react';
// import PropTypes from 'prop-types';
import Hero from './Hero';
import Nav from './Nav';
import '../styles/global.scss';

const Layout = ({
  children,
  hero,
  path,
}) => (
  <>
    <Hero {...hero} />
    <Nav path={path} />
    <main>{children}</main>
  </>
);

// Layout.propTypes = { children: PropTypes.node.isRequired };

export default Layout;
