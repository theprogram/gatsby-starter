import * as React from 'react';
import {
  object,
  shape,
  string,
} from 'prop-types';
import ResImage from '../ResImage';
import * as styles from './Section.module.scss';


const Section = ({
  heading,
  copy,
  asset,
}) => (
  <section className={styles.root}>
    <ResImage {...asset} />
    {heading && <h2>{ heading }</h2>}
    {copy.copy && <p>{ copy.copy }</p>}
  </section>
);

Section.propTypes = {
  heading: string,
  copy: shape({ copy: string.isRequired }),
  asset: shape({
    desktop: object.isRequired,
    mobile: object,
  }),
};

Section.defaultProps = { heading: 'Hello World' };


export default Section;
