import * as React from 'react';
import { string } from 'prop-types';
import { Nav as Links } from '../../constants';
import * as styles from './Nav.module.scss';


const Nav = ({ path }) => {
  const links = Object.entries(Links);

  return (
    <nav className={styles.root}>
      {links.map(([name, slug], i) => (
        <a
          href={slug}
          aria-current={path === slug ? 'page' : undefined}
          key={i}
        >
          {name}
        </a>
      ))}
    </nav>
  );
};

Nav.propTypes = { path: string.isRequired };


export default Nav;
