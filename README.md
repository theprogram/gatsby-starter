# The Program Gatsby Starter

Clone the project and `cd` into it.

Install packages.

```bash
yarn
```

Start local server.

```bash
yarn start
```